/*
You are given a table, Projects, containing three columns: Task_ID, Start_Date and End_Date. 
It is guaranteed that the difference between the End_Date and the Start_Date is equal to 1 day for each row in the table.

Projects:
- Task_ID :: Integer
- Start_Date :: Date
- End_Date :: Date


If the End_Date of the tasks are consecutive, then they are part of the same project. 
Samantha is interested in finding the total number of different projects completed.

Write a query to output the start and end dates of projects listed by the number of days it took to complete the project in ascending order. 
If there is more than one project that have the same number of completion days, then order by the start date of the project.
*/

-- My Solution
/*
Enter your query here.
Please append a semicolon ";" at the end of the query and enter your query in a single line to avoid error.

Output:
- start_date
- end_date
- Order by number of days and start date of the project

Approach
- Define start_data not in end_date and Rank it
- Define end_date not in start_date and Rank it
- Define Datediff
- Order Define Datediff ASC and Print

*/
WITH start_date_rank AS (
    SELECT
        start_date
        , ROW_NUMBER() OVER (ORDER BY start_date) AS seq
    FROM
        Projects
    WHERE
        start_date NOT IN (SELECT end_date FROM Projects)
),

end_date_rank AS (
    SELECT
         end_date
         ,  ROW_NUMBER() OVER (ORDER BY end_date) AS seq
    FROM
        Projects
    WHERE
        end_date NOT IN (SELECT start_date FROM Projects)
)

SELECT 
    start_date
    , end_date
FROM
    start_date_rank
LEFT JOIN end_date_rank
    ON start_date_rank.seq = end_date_rank.seq
ORDER BY
    DATEDIFF(end_date, start_date)
    , start_date

    
