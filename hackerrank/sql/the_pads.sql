/*
Generate the following two result sets:
Query an alphabetically ordered list of all names in OCCUPATIONS, immediately followed by the first letter of each profession as a parenthetical (i.e.: enclosed in parentheses). For example: AnActorName(A), ADoctorName(D), AProfessorName(P), and ASingerName(S).
Query the number of ocurrences of each occupation in OCCUPATIONS. Sort the occurrences in ascending order, and output them in the following format: 
There are a total of [occupation_count] [occupation]s.
where [occupation_count] is the number of occurrences of an occupation in OCCUPATIONS and [occupation] is the lowercase occupation name. If more than one Occupation has the same [occupation_count], they should be ordered alphabetically.
Note: There will be at least two entries in the table for each type of occupation.

Input Format
The OCCUPATIONS table is described as follows:
- Name
- Occupations

Example:
- Samanta / Doctor
- Ron / Actor
- Maria / Professor
*/

-- Solution
WITH agg_occ AS (
    SELECT 
        Occupation
        , COUNT(*) AS total
    FROM
        OCCUPATIONS
    GROUP BY
        Occupation
    ORDER BY 
        total
),

occ_names AS (
    SELECT
        CASE
            WHEN Occupation = 'Doctor' THEN CONCAT(Name,'(D)')
            WHEN Occupation = 'Actor' THEN CONCAT(Name,'(A)')
            WHEN Occupation = 'Singer' THEN CONCAT(Name,'(S)')
            WHEN Occupation = 'Professor' THEN CONCAT(Name,'(P)')
         END AS result
    FROM
         OCCUPATIONS
    ORDER BY 
        result
)

SELECT *
FROM
    occ_names

UNION ALL

SELECT 
    CONCAT('There are a total of ', total, ' ', LOWER(Occupation), 's.') AS result
FROM
    agg_occ
ORDER BY
    result