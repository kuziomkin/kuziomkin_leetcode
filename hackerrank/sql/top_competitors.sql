/*
Julia just finished conducting a coding contest, 
and she needs your help assembling the leaderboard! 
Write a query to print the respective hacker_id and name of hackers who achieved full scores for more than one challenge. 
Order your output in descending order by the total number of challenges in which the hacker earned a full score. 
If more than one hacker received full scores in same number of challenges, then sort them by ascending hacker_id.
*/



/*
Output:
- hacker_id
- name of hackers with full scores for more than one challenges
- order by total number of challenges with full scores desc and hacker_id asc

Approach:
- CTE with full scores users indicators
- COUNT full scores indicators by users
- Order output
*/

WITH joined_challenges AS (
    SELECT 
        s.hacker_id
        , CASE
            WHEN s.score = d.score THEN 1 ELSE 0 
        END AS full_score_indicator
    FROM
        Submissions AS s
    LEFT JOIN Challenges AS c
        ON s.challenge_id = c.challenge_id
    LEFT JOIN Difficulty AS d
        ON c.difficulty_level = d.difficulty_level
),

agg_hackers AS (
    SELECT 
    h.hacker_id
    , h.name
    , SUM(j.full_score_indicator) AS total_full_scores
FROM
    joined_challenges AS j
LEFT JOIN Hackers AS h
    ON j.hacker_id = h.hacker_id
GROUP BY
    h.name
    , h.hacker_id
HAVING
    SUM(j.full_score_indicator) > 1
)

SELECT
    hacker_id
    , name
FROM
    agg_hackers
ORDER BY
    total_full_scores DESC
    , hacker_id ASC
