/*
Pivot the Occupation column in OCCUPATIONS so that each Name is sorted alphabetically and displayed underneath its corresponding Occupation. The output column headers should be Doctor, Professor, Singer, and Actor, respectively.
Note: Print NULL when there are no more names corresponding to an occupation.

Input Format
The OCCUPATIONS table is described as follows:
- Name
- Occupation

Occupation will only contain one of the following values: Doctor, Professor, Singer or Actor.

Sample Output
Jenny    Ashley     Meera  Jane
Samantha Christeen  Priya  Julia
NULL     Ketty      NULL   Maria
*/

--Solution
/*
Enter your query here.
*/

WITH occ_agg AS (
    SELECT 
        Name
        , Occupation
        , RANK() OVER (PARTITION BY Occupation Order BY Name) AS rk
    FROM
        OCCUPATIONS
),

doctor_filtered AS (
    SELECT *
    FROM
        occ_agg
    WHERE
        Occupation = 'Doctor'
),

actor_filtered AS (
    SELECT *
    FROM
        occ_agg
    WHERE
        Occupation = 'Actor'
),

singer_filtered AS (
    SELECT *
    FROM
        occ_agg
    WHERE
        Occupation = 'Singer'
),

professor_filtered AS (
    SELECT *
    FROM
        occ_agg
    WHERE
        Occupation = 'Professor'
)

SELECT DISTINCT
    df.Name AS doctor
    , pf.Name AS professor
    , sf.Name AS singer
    , af.Name AS actor
FROM
    occ_agg AS oa
LEFT JOIN doctor_filtered AS df
    ON oa.rk = df.rk
LEFT JOIN professor_filtered AS pf
    ON oa.rk = pf.rk
LEFT JOIN singer_filtered AS sf
    ON oa.rk = sf.rk
LEFT JOIN actor_filtered AS af
    ON oa.rk = af.rk




    

