/*
You did such a great job helping Julia with her last coding contest challenge that she wants you to work on this one, too!

The total score of a hacker is the sum of their maximum scores for all of the challenges. 
Write a query to print the hacker_id, name, and total score of the hackers ordered by the descending score. 
If more than one hacker achieved the same total score, then sort the result by ascending hacker_id. 
Exclude all hackers with a total score of  from your result.

Hackers:
- hacker_id
- name

Submissions:
- submission_id
- hacker_id
- challenge_id
- score
*/

-- My Solutin
/*
Cardinality:
- one hacker might have multiple submissions


Output
- hacker_id
- name
- total score of max sumbmission by hacker
- order by score DESC and hacker_id ASC

Constraints:
- if total score is zero then exclude hackers

Approach:
- CTE create agg_submissions table with max submission of challenges per hacker
- Join hackers table with CTE and SUM max agg_submissions per hacker having count > 0
- Order output

*/

WITH 
    agg_submissions AS (
        SELECT
            hacker_id
            , challenge_id
            , MAX(score) AS max_score
        FROM
            Submissions
        GROUP BY
            hacker_id
            , challenge_id
    )

SELECT
    h.hacker_id
    , name
    , SUM(s.max_score) AS total_score
FROM
    Hackers AS h
LEFT JOIN agg_submissions AS s
    ON h.hacker_id = s.hacker_id
GROUP BY
     h.hacker_id
    , name
HAVING
    SUM(s.max_score) > 0
ORDER BY
    SUM(s.max_score) DESC
    , hacker_id
