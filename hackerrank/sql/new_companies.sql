/*
Amber's conglomerate corporation just acquired some new companies. Each of the companies follows this hierarchy: 
Given the table schemas below, write a query to print the company_code, founder name, total number of lead managers, total number of senior managers, total number of managers, and total number of employees. Order your output by ascending company_code.

Note:
- The tables may contain duplicate records.
- The company_code is string, so the sorting should not be numeric. For example, if the company_codes are C_1, C_2, and C_10, then the ascending company_codes will be C_1, C_10, and C_2.

Input Format
The following tables contain company data:

Company: 
- company_code
- founder

Lead_Manager:
- lead_manager_code
- company_code

Senior_Manager
- senior_manager_code
- lead_manager_code
- company_code

Manager:
- manager_code
- senior_manager_code
- lead_manager_code
- company_code

Employee:
- employee_code
- manager_code
- senior_manager_code
- lead_manager_code
- company_code
*/

-- SOLUTION
SELECT 
    c.company_code
    , c.founder
    , COUNT(DISTINCT lm.lead_manager_code) AS total_lead
    , COUNT(DISTINCT sm.senior_manager_code) AS total_senior
    , COUNT(DISTINCT m.manager_code) AS total_manager
    , COUNT(DISTINCT e.employee_code) AS total_employee
FROM
    Company AS c 
LEFT JOIN Lead_Manager AS lm
    ON c.company_code = lm.company_code
LEFT JOIN Senior_Manager AS sm
    ON lm.lead_manager_code = sm.lead_manager_code
LEFT JOIN Manager AS m
    ON sm.senior_manager_code = m.senior_manager_code
LEFT JOIN Employee AS e
    ON m.manager_code = e.manager_code
GROUP BY
    c.company_code
    , c.founder
ORDER BY
    company_code


