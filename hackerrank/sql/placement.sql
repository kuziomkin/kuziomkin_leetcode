/*
You are given three tables: Students, Friends and Packages. 
Students contains two columns: ID and Name. Friends contains two columns: ID and Friend_ID (ID of the ONLY best friend). 
Packages contains two columns: ID and Salary (offered salary in $ thousands per month).

Students:
- ID
- Name

Friends:
- ID
- Friends_ID

Packages:
- ID
- Salary

Write a query to output the names of those students whose best friends got offered a higher salary than them. 
Names must be ordered by the salary amount offered to the best friends. 
It is guaranteed that no two students got same salary offer.
*/

-- My Solution
/*
Cardinality:
- Students to Friends as one to one
- Students to Packages as one to one

Output
- names of studetns with less salary than his friends
- order names by salary amount of the best friends

Constraints:
- There are no two students with the same salary

Approach:
- CTE with students salary and CTE with friends salary
- JOIN CTE students and friends
- Select students with lower salary than his friends
- Order names by salary amount of the friends ASC
*/

WITH students_salary AS (
    SELECT 
        s.ID
        , s.Name
        , p.Salary
    FROM
        Students AS s
    LEFT JOIN Packages AS p
        ON s.ID = p.ID
),

friends_salary AS (
    SELECT
        f.ID
        , f.Friend_ID
        , p.Salary
    FROM
        Friends AS f
    LEFT JOIN Packages AS p
        ON f.Friend_ID = p.ID
)

SELECT 
    ss.Name
FROM
    students_salary AS ss
LEFT JOIN friends_salary AS fs
    ON ss.ID = fs.ID
WHERE
    ss.Salary < fs.Salary
ORDER BY
    fs.Salary



