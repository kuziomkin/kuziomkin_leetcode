/*
Query the two cities in STATION with the shortest and longest CITY names, as well as their respective lengths (i.e.: number of characters in the name). If there is more than one smallest or largest city, choose the one that comes first when ordered alphabetically. 
The STATION table is described as follows:
- ID
- CITY
- STATE
- LAT_N
- LONG_W

Sample Input
For example, CITY has four entries: DEF, ABC, PQRS and WXY.
Sample Output
ABC 3
PQRS 4

Explanation
When ordered alphabetically, the CITY names are listed as ABC, DEF, PQRS, 
and WXY, with lengths  and . The longest name is PQRS, but there are options for shortest named city. Choose ABC, because it comes first alphabetically.
*/

--Solution
/*
Enter your query here.
*/
WITH city_len AS (
    SELECT 
        CITY
        , LENGTH(CITY) AS len
    FROM
        STATION
    ORDER BY
        CITY
),

max_min AS (
    SELECT 
        MAX(len) AS max_len
        , MIN(len) AS min_len
    FROM
        city_len
),

max_res AS (
    SELECT 
        city_len.CITY
        , city_len.len
    FROM
        city_len
        , max_min
    WHERE
        city_len.len = max_min.max_len
    ORDER BY
        city_len.CITY
    LIMIT 1
),

min_res AS (
    SELECT 
        city_len.CITY
        , city_len.len
    FROM
        city_len
        , max_min
    WHERE
        city_len.len = max_min.min_len
    ORDER BY
        city_len.CITY
    LIMIT 1
)

SELECT *
FROM
    max_res

UNION ALL

SELECT *
FROM
    min_res


