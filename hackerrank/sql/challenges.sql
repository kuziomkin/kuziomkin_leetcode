/*
Julia asked her students to create some coding challenges. 
Write a query to print the hacker_id, name, and the total number of challenges created by each student. 
Sort your results by the total number of challenges in descending order. 
If more than one student created the same number of challenges, then sort the result by hacker_id. 
If more than one student created the same number of challenges and the count is less than the maximum number of challenges created, then exclude those students from the result

Hackers:
- hacker_id
- name

Challenges:
- challenge_id
- hacker_id
*/


-- My Solution
/*
Cordinality:
- Hackers to Challenges as one to many

Output:
- hacker_id and name
- total number of challenges by student
- Order by total_number of challenges DESC and Order by hacker_id ASC

Constrants:
- if more than one student create the same number of challenges and count is less than maximum than exclude students from the result

Approach:
- CTE total challenges by hacker
- CTE MAX total challenges
- WF COUNT total_challenges
- CASE COUNT total_challenges > 1 and != Max total THEN hacker_id as excluded
- Output hackers with total_challenges and filter excluded
*/

WITH agg_challenges AS (
    SELECT
        hacker_id
        , COUNT(challenge_id) AS total_challenge
    FROM
        Challenges
    GROUP BY
        hacker_id
),

max_challenges AS (
    SELECT
        hacker_id
        , total_challenge
        , COUNT(*) OVER (PARTITION BY total_challenge) AS total_count
        , MAX(total_challenge) OVER () AS max_challenge
    FROM
        agg_challenges
),

excluded_id AS (
    SELECT
        CASE
            WHEN total_count > 1 AND total_challenge < max_challenge THEN hacker_id
        END AS excluded
    FROM
        max_challenges
)

SELECT
  h.hacker_id
  , h.name
  , mc.total_challenge
FROM
    Hackers AS h
LEFT JOIN max_challenges AS mc
  ON h.hacker_id = mc.hacker_id
LEFT JOIN excluded_id AS e
   ON h.hacker_id = e.excluded
WHERE
   e.excluded IS NULL
   AND mc.total_challenge IS NOT NULL
ORDER BY
    mc.total_challenge DESC
    , h.hacker_id ASC
