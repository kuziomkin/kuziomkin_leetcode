/*
You are given two tables: Students and Grades. 
Students contains three columns ID, Name and Marks.

Ketty gives Eve a task to generate a report containing three columns: Name, Grade and Mark. 
Ketty doesn't want the NAMES of those students who received a grade lower than 8. 
The report must be in descending order by grade -- i.e. higher grades are entered first. 
If there is more than one student with the same grade (8-10) assigned to them, order those particular students by their name alphabetically. 
Finally, if the grade is lower than 8, use "NULL" as their name and list them by their grades in descending order. 
If there is more than one student with the same grade (1-7) assigned to them, 
order those particular students by their marks in ascending order.

Write a query to help Eve.
*/

/*
My solution
*/

WITH students_grade AS (
    SELECT
        name
        , CASE 
            WHEN marks >= 0 AND marks < 10 THEN 1
            WHEN marks >= 10 AND marks < 20 THEN 2
            WHEN marks >= 20 AND marks < 30 THEN 3
            WHEN marks >= 30 AND marks < 40 THEN 4
            WHEN marks >= 40 AND marks < 50 THEN 5
            WHEN marks >= 50 AND marks < 60 THEN 6
            WHEN marks >= 60 AND marks < 70 THEN 7
            WHEN marks >= 70 AND marks < 80 THEN 8
            WHEN marks >= 80 AND marks < 90 THEN 9
            WHEN marks >= 90 AND marks <= 100 THEN 10
        END AS grade
        , marks
    FROM
        Students
),

grade_more AS (
    SELECT 
        name 
        , grade
        , marks
        , RANK() OVER (ORDER BY grade DESC, name ASC) AS rk
    FROM
        students_grade
    WHERE
        grade >= 8

),
  
grade_less AS (
    SELECT
        NULL AS name
        , grade
        , marks
        , RANK() OVER (ORDER BY grade DESC, marks ASC) AS rk
    FROM
        students_grade
    WHERE
        grade < 8
    ORDER BY
        grade DESC
)

SELECT 
 name
 , grade
 , marks
FROM
    grade_more
    
UNION ALL

SELECT 
 name
 , grade
 , marks
FROM
    (SELECT 
         name
         , grade
         , marks
         , rk*100 AS rk
        FROM grade_less
        ORDER BY
            rk
    ) AS SUB




