/*
We define an employee's total earnings to be their monthly  worked, and the maximum total earnings to be the maximum total earnings for any employee in the Employee table. Write a query to find the maximum total earnings for all employees as well as the total number of employees who have maximum total earnings. 
Then print these values as  space-separated integers

Employee:
- employee_id
- name
- months
- salary
*/

-- My Solution

/*

Output:
- salary*months per employee
- Max earning for all employee
- Count empliyee with max earning
- Print max earnings with count of employee in one string. Example: 1600 2

Approach
1. Define total ernings per employee (CTE)
2. Define Max erning per employee
3. COUNT employee with max ernings
4. Print result
*/

WITH 
    --total erning
    agg_employee AS (
        SELECT 
            employee_id
            , (months * salary) AS total_salary
        FROM
            Employee
    ),
    --max erning
    max_earnings AS (
        SELECT
            MAX(total_salary) AS max_earning
        FROM
            agg_employee
    ),
    --total employee with max ernings
    count_employee_max AS (
        SELECT
            COUNT(employee_id) AS total_max_empl
        FROM
            agg_employee
        WHERE
            total_salary IN (SELECT * FROM max_earnings)
    )
--print results
SELECT
    (SELECT * FROM max_earnings) AS max_earning
    , (SELECT * FROM count_employee_max) AS total_empl
