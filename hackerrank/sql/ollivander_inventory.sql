/*
Harry Potter and his friends are at Ollivander's with Ron, finally replacing Charlie's old broken wand.

Hermione decides the best way to choose is by determining the minimum number of gold galleons needed to buy each non-evil wand of high power and age. 
Write a query to print the id, age, coins_needed, and power of the wands that Ron's interested in, sorted in order of descending power. 
If more than one wand has same power, sort the result in order of descending age.

Wands:
- id
- code
- coins_needed
- power

Wands_Property:
- code
- age 
- is_evil
*/

-- My Solution
/*

Taks:
- find mind coins in age and power group

Cardinality:
- wand has one code

Output:
- id
- age
- coins_needed
- power
- order by power DESC and age DESC

Approach:
- Join tables
- Filter out evil wands
- Order results
*/

WITH agg_wands AS (
    SELECT
        w.id
        , wp.age
        , w.coins_needed
        , MIN(w.coins_needed) OVER (PARTITION BY wp.age, w.power) AS min_coins
        , w.power
    FROM
        Wands AS w
    LEFT JOIN Wands_Property AS wp
        ON w.code = wp.code
    WHERE
        wp.is_evil = 0
)

SELECT 
    id
    , age
    , coins_needed
    , power
FROM
   agg_wands 
WHERE
    coins_needed = min_coins
ORDER BY
    power DESC
    , age DESC