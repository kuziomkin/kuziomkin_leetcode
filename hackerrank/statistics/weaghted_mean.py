""""
Objective 
In the previous challenge, we calculated a mean. In this challenge, we practice calculating a weighted mean.
Check out the Tutorial tab for learning materials and an instructional video!
Task 
Given an array, X, N of  integers and an array, W 
, representing the respective weights of 's elements, calculate and print the weighted mean of 's elements.
 Your answer should be rounded to a scale of  decimal place (i.e.,  format).
"""

#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'weightedMean' function below.
#
# The function accepts following parameters:
#  1. INTEGER_ARRAY X
#  2. INTEGER_ARRAY W
#

def weightedMean(X, W):
    # Write your code here
    weighted_vals = [i*n for i,n in zip(X,W)]
    weighted_total = 0
    weighted_vals_total = 0
    
    for i,n in zip(W, weighted_vals):
        weighted_total += i
        weighted_vals_total += n
    weighted_mean = round(weighted_vals_total / weighted_total,1)
    
    return weighted_mean

if __name__ == '__main__':
    n = int(input().strip())

    vals = list(map(int, input().rstrip().split()))

    weights = list(map(int, input().rstrip().split()))

    print(weightedMean(vals, weights))

