""""
Objective
In this challenge, we practice calculating standard deviation. 
Check out the Tutorial tab for learning materials and an instructional video!

Task
Given an array X, N, of  integers, calculate and print the standard deviation. 
Your answer should be in decimal form, rounded to a scale of  decimal place (i.e.,  format). 
An error margin of  will be tolerated for the standard deviation.
"""

def get_mean(arr):
    mean = 0
    for i in arr:
        mean += i
    return round(mean/len(arr),1)

def stdDev(arr):
    mean = get_mean(arr)
    arr_sum_mul = 0
    for i in arr:
        arr_sum_mul += (i - mean)**2
    stdr =  math.sqrt(arr_sum_mul / len(arr))
    return round(stdr,1)