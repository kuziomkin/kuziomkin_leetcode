""""
Objective 
In this challenge, we practice calculating quartiles. 
Check out the Tutorial tab for learning materials and an instructional video!
Task 
Given an array X, N of  integers, calculate the respective 
first quartile (), second quartile (), 
and third quartile (). It is guaranteed that N are integers.
"""

def quartiles(arr):
    # sort list
    arr.sort()
    #create placeholders
    q1 = 0
    q2 = 0
    q3 = 0
    if len(arr) % 2 != 0:
        q2 = arr[len(arr) // 2]
        new_list1 = arr[0:arr.index(q2)]
        new_list2 = arr[arr.index(q2)+1:]
    else:
        q2 = (arr[len(arr)//2] + arr[(len(arr)//2)-1])//2
        new_list1 = []
        for i in arr:
            if i < q2:
                new_list1.append(i)    
        new_list2 = []
        for i in arr:
            if i > q2:
                new_list2.append(i)
                
    if len(new_list1) % 2 != 0:
        q1 = new_list1[len(new_list1) // 2]
    else:  
        q1 = (new_list1[len(new_list1) // 2] + new_list1[(len(new_list1) // 2)-1])//2
    if len(new_list2) % 2 != 0:
        q3 = new_list2[len(new_list2) // 2]
    else:
        q3 = (new_list2[len(new_list2) // 2] + new_list2[(len(new_list2) // 2)-1])//2
                
    return q1, q2, q3