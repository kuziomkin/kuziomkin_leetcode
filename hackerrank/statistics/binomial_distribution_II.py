""""
Objective
In this challenge, we go further with binomial distributions. 
We recommend reviewing the previous challenge's Tutorial before attempting this problem.

Task
A manufacturer of metal pistons finds that, on average, 12%  of the pistons they manufacture are rejected because they are incorrectly sized. 
What is the probability that a batch of 10 pistons will contain:

No more than 2 rejects?
At least 2 rejects?

Round both of your answers to a scale of 3 decimal places (
"""

#Solution
# Enter your code here. Read input from STDIN. Print output to STDOUT

import math

def no_more_two(prob=12, batch=10, not_rej=8):
    rej_prob = prob/100
    not_rej_prob = 1 - rej_prob
    #find not rejected probability
    rej = batch - not_rej
    prob = (math.factorial(batch)/(math.factorial(not_rej)*(math.factorial(rej)))) * (not_rej_prob ** not_rej) * (rej_prob ** (rej))
    return prob
    

print((round(no_more_two() + no_more_two(not_rej=9) + no_more_two(not_rej=10),3)))

print(round(no_more_two(not_rej=8) + no_more_two(not_rej=7) + no_more_two(not_rej=6) + no_more_two(not_rej=5) + no_more_two(not_rej=4) + no_more_two(not_rej=3) + no_more_two(not_rej=2) + no_more_two(not_rej=1) + no_more_two(not_rej=0),3))