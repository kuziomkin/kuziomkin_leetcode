""""
Objective 
In this challenge, we practice calculating the interquartile range. We recommend you complete the Quartiles challenge before attempting this problem.
Task 
The interquartile range of an array is the difference between its first () and third () quartiles (i.e., ).
Given an array, , of  integers and an array, , representing the respective frequencies of 's elements, construct a data set, , where each  occurs at frequency . 
Then calculate and print 's interquartile range, rounded to a scale of  decimal place (i.e.,  format).
Tip: Be careful to not use integer division when averaging the middle two elements for a data set with an even number of elements, 
and be sure to not include the median in your upper and lower data sets.

"""
#
# Complete the 'interQuartile' function below.
#
# The function accepts following parameters:
#  1. INTEGER_ARRAY values
#  2. INTEGER_ARRAY freqs
#
def get_mid(arr:list) -> float:
    arr.sort()
    if len(arr) % 2 == 0:
        mid = (arr[len(arr)//2] + arr[(len(arr)//2)-1])/2
    else:
        mid = arr[len(arr)//2]
    return round(mid,1)

def interQuartile(values, freqs):
    #define the full list
    full_list = []
    for v, f in zip(values,freqs):
        for t in range(f):
            full_list.append(v)
    full_list.sort()
    q2 = get_mid(full_list)
    q1_list = []
    q3_list = []
    if len(full_list) % 2 == 0:
        for i in full_list:
            if i < q2:
                q1_list.append(i)
            else:
                q3_list.append(i)
    else:
        q1_list = full_list[0 :len(full_list) // 2]
        q3_list = full_list[(len(full_list) // 2) +1 : ]
    q1 = get_mid(q1_list)
    q3 = get_mid(q3_list)
    #result
    return float(q3 - q1)
