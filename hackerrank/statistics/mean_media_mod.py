''''
Objective 
In this challenge, we practice calculating the mean, median, and mode. Check out the Tutorial tab for learning materials and an instructional video!
Task 
Given an array, , of  integers, calculate and print the respective mean, median, and mode on separate lines. If your array contains more than one modal value, choose the numerically smallest one.
Note: Other than the modal value (which will always be an integer), your answers should be in decimal form, rounded to a scale of  decimal place (i.e., ,  format).

Input Format
The first line contains an integer, , the number of elements in the array. 
The second line contains  space-separated integers that describe the array's elements.

Output Format
Print  lines of output in the following order:
Print the mean on the first line to a scale of  decimal place (i.e., , ).
Print the median on a new line, to a scale of  decimal place (i.e., , ).
Print the mode on a new line. If more than one such value exists, print the numerically smallest one.
'''

# Enter your code here. Read input from STDIN. Print output to STDOUT
# Enter your code here. Read input from STDIN. Print output to STDOUT
# Enter your code here. Read input from STDIN. Print output to STDOUT

n = int(input())
x = list(map(int, input().split()))

def get_mean(n:int, x: list) -> float:
    
    #define mean
    mean_result = None
    x_sum = 0
    for i in x:
        x_sum += i
    mean_result = x_sum / n
    
    return round(mean_result,1)


def get_med(n:int, x: list) -> float:
    #define med
    x.sort()
    if n % 2 == 0:
        med_result = (x[n // 2] + x[(n // 2)-1]) / 2
    else:
        med_result = x[n // 2]
    
    return round(med_result,1)


def get_mod(n:int, x: list) -> int:
    #define mod
    x.sort()
    el_dic = {i: 0 for i in x}
    for i in x:
        if i in el_dic:
            el_dic[i] += 1
    
    max = 1
    mod_result = x[0]
    for key, value in el_dic.items():
        if value > max:
            max = value
            mod_result = key
    return mod_result
    
print(get_mean(n, x))
print(get_med(n, x))
print(get_mod(n, x))
