''''
Objective
In this challenge, we learn about binomial distributions. 
Check out the Tutorial tab for learning materials!

Task
The ratio of boys to girls for babies born in Russia is 1.09 / 1. 
If there is  child born per birth, what proportion of Russian families with exactly 6 children will have at least 3 boys?

Write a program to compute the answer using the above parameters. 
Then print your result, rounded to a scale of  decimal places (i.e.,  format).
'''

# Enter your code here. Read input from STDIN. Print output to STDOUT

def fact(x: int) -> int:
    result = 0
    if x > 1:
        result = x
        for i in range(x-1, 0, -1):
            result = result * i
    elif x < 0:
        result = 0
    else:
        result = x
        
    return result
    
def binomial_distr(p=1.09/2.09, n=6, x=3):
    q = 1 - p
    if n != x:
        result = (fact(n) / (fact(x) * fact((n-x)))) * (p**x) * (q**(n-x))
    else:
        result = (fact(n) / (fact(x))) * (p**x) * (q**(n-x))
    return round(result, 3)

#print(binomial_distr(x=6) + binomial_distr(x=5) + binomial_distr(x=4) + binomial_distr(x=3))

import math
from decimal import Decimal

def no_more_two(prob=12, batch=10, not_rej=8):
    rej_prob = prob/100
    not_rej_prob = 1 - rej_prob
    #find not rejected probability
    rej = batch - not_rej
    prob = (math.factorial(batch)/(math.factorial(not_rej)*(math.factorial(rej)))) * (not_rej_prob ** not_rej) * (rej_prob ** (rej))
    return prob
    

print((round(no_more_two() + no_more_two(not_rej=9) + no_more_two(not_rej=10),3)))

print(round(no_more_two(not_rej=8) + no_more_two(not_rej=7) + no_more_two(not_rej=6) + no_more_two(not_rej=5) + no_more_two(not_rej=4) + no_more_two(not_rej=3) + no_more_two(not_rej=2) + no_more_two(not_rej=1) + no_more_two(not_rej=0),3))