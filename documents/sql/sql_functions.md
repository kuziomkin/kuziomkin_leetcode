# The LAG Function and the LEAD Function in SQL

The LAG() function allows access to a value stored in a different row above the current row. The row above may be adjacent or some number of rows above, as sorted by a specified column or set of columns.

```sql
LAG(expression [,offset[,default_value]]) OVER(ORDER BY columns)
```

LAG() takes three arguments: the name of the column or an expression from which the value is obtained, the number of rows to skip (offset) above, and the default value to be returned if the stored value obtained from the row above is empty. Only the first argument is required. The third argument (default value) is allowed only if you specify the second argument, the offset.

As with other window functions, LAG() requires the OVER clause. It can take optional parameters, which we will explain later. With LAG(), you must specify an ORDER BY in the OVER clause, with a column or a list of columns by which the rows should be sorted.

```sql
SELECT seller_name, sale_value,
  LAG(sale_value) OVER(ORDER BY sale_value) as previous_sale_value
FROM sale;
```

As you can see, this table contains the total sale amount by year. Using LAG() and LEAD(), we can compare annual sale amounts across years.

Example:

```sql
SELECT  year, current_total_sale,
   LAG(total_sale) OVER(ORDER BY year) AS previous_total_sale,
   total_sale - LAG(total_sale) OVER(ORDER BY year) AS difference
FROM annual_sale;
```

|year|total_sale|previous_total_sale|difference|
|---|---|---|---|
|2015|23000|NULL|NULL|
|2016|25000|23000|2000|
|2017|34000|25000|9000|

# The ROW_NUMBER Function, RANK and the DENSE_RANK Function in SQL

## ROW_NUMBER()

We use ROW_Number() SQL RANK function to get a unique sequential number for each row in the specified data. It gives the rank one for the first row and then increments the value by one for each row. We get different ranks for the row having similar values as well.

```sql
SELECT 
    Studentname,
    Subject,
    Marks,
    ROW_NUMBER() OVER (ORDER BY Marks) RowNumber
FROM ExamResult;
```

Outcomes:

|Studentname|Subject|Marks|RowNumber|
|---|---|---|---|
|Isabel|Math|50|1|
|Olivia|Math|55|2|
|Lily|English|70|3|
|Sirat|Math|70|4|

## RANK()

We use RANK() SQL Rank function to specify rank for each row in the result set. We have student results for three subjects. We want to rank the result of students as per their marks in the subjects.

```sql
SELECT 
    Studentname,
    Subject,
    Marks,
    RANK() OVER (PARTITION BY Studentname ORDER BY Marks) 'Rank'
FROM ExamResult
ORDER BY 
    Studentname
```
Outcomes:

|Studentname|Subject|Marks|RowNumber|
|---|---|---|---|
|Lily|Math|90|1|
|Lily|English|70|2|
|Lily|Science|65|3|
|Sirat|Math|70|1|
|Sirat|English|55|2|

## DENSE_RANK()

We use DENSE_RANK() function to specify a unique rank number within the partition as per the specified column value. It is similar to the Rank function with a small difference.

In the SQL RANK function DENSE_RANK(), if we have duplicate values, SQL assigns different ranks to those rows as well. Ideally, we should get the same rank for duplicate or similar values.

```sql
SELECT 
    Studentname,
    Subject,
    Marks,
    DENSE_RANK() OVER (ORDER BY Marks) 'Rank'
FROM 
    ExamResult
ORDER BY 
    Rank;
```

Outcomes:

|Studentname|Subject|Marks|RowNumber|
|---|---|---|---|
|Isabel|Math|50|1|
|Olivia|Math|55|2|
|Lily|English|70|3|
|Sirat|Math|70|3|

# SQL PARTITION BY

We can use the SQL PARTITION BY clause with the OVER clause to specify the column on which we need to perform aggregation. In the previous example, we used Group By with CustomerCity column and calculated average, minimum and maximum values.

Let us rerun this scenario with the SQL PARTITION BY clause using the following query.

```sql
SELECT Customercity
    AVG(OrderAmount) OVER (PARTITION BY Customercity) AS AvgOrderAmount,
    MIN(OrderAmount) OVER (PARTITION BY Customercity) AS MinOrderAmount,
    SUM(OrderAmount) OVER (PARTITION BY Customercity) AS TotalOrderAmount
FROM Orders
```

Outcome:

|Customercity|AvgOrderAmount|MinOrderAmount|TotalOrderAmount|
|---|---|---|---|
|Austin|1631|936|2354|
|Chicago|1622|1011|3442|
|Columbia|5432|1222|24255|

# DATE_ADD() Function

The DATE_ADD() function adds a time/date interval to a date and then returns the date.

```sql
SELECT DATE_ADD("2017-06-15", INTERVAL 10 DAY);
```

Syntax

```sql
DATE_ADD(date, INTERVAL value addunit)
```

# DATE_SUB() Function

The DATE_SUB() function subtracts a time/date interval from a date and then returns the date.

```sql
SELECT DATE_SUB("2017-06-15", INTERVAL 10 DAY);
```

Syntax

```sql
DATE_SUB(date, INTERVAL value interval)
```

Parameter Values:

|Parameter|Value|
|---|---|
|date|Required. The date to be modified|
|value|Required. The value of the time/date interval to add. Both positive and negative values are allowed|
|addunit|Required. The type of interval to add. Can be one of the following values: <br> MICROSECOND <br> SECOND <br> MINUTE <br> HOUR <br> DAY <br> WEEK <br> MONTH <br> QUARTER <br> YEAR <br> SECOND_MICROSECOND <br> MINUTE_MICROSECOND <br> MINUTE_SECOND <br> HOUR_MICROSECOND <br> HOUR_SECOND <br> HOUR_MINUTE <br> DAY_MICROSECOND <br> DAY_SECOND <br> DAY_MINUTE <br> DAY_HOUR <br> YEAR_MONTH| 

# IF() Function

Returns expr1 if cond is true, or expr2 otherwise.

```sql
If(cond, expr1, expr2)
```

Arguments

|Argument|Description|
|---|---|
|cond|A BOOLEAN expression|
|expr1|An expression of any type|
|expr2|An expression that shares a least common type with expr1|

# COALESCE( ) Function

he MySQL COALESCE() function is used for returning the first non-null value in a list of expressions. If all the values in the list evaluate to NULL, then the COALESCE() function returns NULL.

The COALESCE() function accepts one parameter which is the list which can contain various values. The value returned by the MySQL COALESCE() function is the first non-null value in a list of expressions or NULL if all the values in a list are NULL.

```sql
SELECT COALESCE(NULL, 'A', 'B', NULL); 
```

Output:

```
'A'
```