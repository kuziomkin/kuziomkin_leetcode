"""
Write a Python program that accepts a comma separated sequence of words as input and prints the unique words in sorted form.
Examples:
Input: ['red', 'white', 'black', 'red', 'green', 'black']
Output: ['black', 'green', 'red', 'white']

"""

def sort_seq(seq: list):
    result = []
    for i in seq:
        if i not in result:
            result.append(i)
    result.sort()
    return result

print(sort_seq(['red', 'white', 'black', 'red', 'green', 'black']))