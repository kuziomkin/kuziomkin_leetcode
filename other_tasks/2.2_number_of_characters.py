"""
Task 2.2
Write a Python program to count the number of characters
(character frequency) in a string (ignore case of letters).
Examples:
```
Input: 'Oh, it is python' 
Output: {',': 1, ' ': 3, 'o': 2, 'h': 2, 'i': 2, 't': 2, 's': 1, 'p': 1, 'y': 1, 'n': 1}
"""
def count_chart(st):
    dic = {}
    st = st.lower()
    for n in st:
        count = 0
        for i in range(len(st)):
            if n == st[i]:
                count += 1
        dic.update({n : count})
    return dic

print(count_chart('Oh, it is python'))


        
        

        

