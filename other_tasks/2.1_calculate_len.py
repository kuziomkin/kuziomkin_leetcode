
"""
Task 2.1
Write a Python program to calculate the length of a string
without using the `len` function.
"""

def calculate_len(st):
    count = 0
    for l in st:
        count += 1
    return count


print(calculate_len('Hello'))