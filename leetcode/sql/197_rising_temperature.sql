/*
Write an SQL query to find all dates' Id with higher temperatures compared to its previous dates (yesterday).

Return the result table in any order.

Table: Weather

+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| recordDate    | date    |
| temperature   | int     |
+---------------+---------+
id is the primary key for this table.
This table contains information about the temperature on a certain day.

Example 1:

Input: 
Weather table:
+----+------------+-------------+
| id | recordDate | temperature |
+----+------------+-------------+
| 1  | 2015-01-01 | 10          |
| 2  | 2015-01-02 | 25          |
| 3  | 2015-01-03 | 20          |
| 4  | 2015-01-04 | 30          |
+----+------------+-------------+
Output: 
+----+
| id |
+----+
| 2  |
| 4  |
+----+
Explanation: 
In 2015-01-02, the temperature was higher than the previous day (10 -> 25).
In 2015-01-04, the temperature was higher than the previous day (20 -> 30).
*/

-- My solution 1
SELECT
    w1.id
FROM 
    Weather AS w1,
    Weather AS w2
WHERE 
    w1.recordDate = DATE_ADD(w2.recordDate, INTERVAL 1 DAY)
    AND w1.temperature > w2.temperature


-- My solution 2
SELECT
    id
FROM 
    (
        SELECT
            id,
            temperature,
            recordDate,
            LAG(recordDate) OVER (ORDER BY recordDate) AS last_date,
            LAG(temperature) OVER (ORDER BY recordDate) AS last_date_temp
        FROM 
            Weather    
    ) AS sub
WHERE
    temperature > last_date_temp
    AND recordDate = DATE_ADD(last_date, INTERVAL 1 DAY)