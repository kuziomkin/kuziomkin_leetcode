/*
Write an SQL query to report the n-th highest salary from the Employee table. If there is no n-th highest salary, the query should report null.

The query result format is in the following example. 

Table: Employee
+-------------+------+
| Column Name | Type |
+-------------+------+
| id          | int  |
| salary      | int  |
+-------------+------+
id is the primary key column for this table.
Each row of this table contains information about the salary of an employee.

Example:
Input: 
Employee table:
+----+--------+
| id | salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
n = 2
Output: 
+------------------------+
| getNthHighestSalary(2) |
+------------------------+
| 200                    |
+------------------------+

Input: 
Employee table:
+----+--------+
| id | salary |
+----+--------+
| 1  | 100    |
+----+--------+
n = 2
Output: 
+------------------------+
| getNthHighestSalary(2) |
+------------------------+
| null                   |
+------------------------+
*/

CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT 
BEGIN 
    DECLARE skip INT; 
    SET skip = N - 1; 
  RETURN ( 
      # Write your MySQL query statement below. 
      SELECT 
        IFNULL( 
                ( 
                SELECT DISTINCT salary 
                FROM Employee 
                ORDER BY salary DESC 
                LIMIT 1 OFFSET skip 
                ),  
                NULL               
            ) 
  ); 
END