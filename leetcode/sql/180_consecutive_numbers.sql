/*
Write an SQL query to find all numbers that appear at least three times consecutively.

Return the result table in any order.

The query result format is in the following example.

Table: Logs
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| id          | int     |
| num         | varchar |
+-------------+---------+
id is the primary key for this table.
id is an autoincrement column.

Example:
Input: 
Logs table:
+----+-----+
| id | num |
+----+-----+
| 1  | 1   |
| 2  | 1   |
| 3  | 1   |
| 4  | 2   |
| 5  | 1   |
| 6  | 2   |
| 7  | 2   |
+----+-----+
Output: 
+-----------------+
| ConsecutiveNums |
+-----------------+
| 1               |
+-----------------+
Explanation: 1 is the only number that appears consecutively for at least three times.
*/

SELECT
    DISTINCT(num) AS ConsecutiveNums
FROM
        (SELECT
            num,
            COUNT(id) OVER (PARTITION BY num) AS count_num,
            LAG(num) OVER (ORDER BY id) AS prev_num,
            LEAD(num) OVER (ORder BY id) AS next_num
        FROM Logs 
        ) AS sub
WHERE 
    count_num >= 3 
    AND num = prev_num
    AND num = next_num;