/*
Write an SQL query to report the second highest salary from the Employee table. If there is no second highest salary, the query should report null.
The query result format is in the following example.

Employee table
+-------------+------+
| Column Name | Type |
+-------------+------+
| id          | int  |
| salary      | int  |
+-------------+------+
id is the primary key column for this table.
Each row of this table contains information about the salary of an employee.


Example
Input: 
Employee table:
+----+--------+
| id | salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
Output: 
+---------------------+
| SecondHighestSalary |
+---------------------+
| 200                 |
+---------------------+

Input: 
Employee table:
+----+--------+
| id | salary |
+----+--------+
| 1  | 100    |
+----+--------+
Output: 
+---------------------+
| SecondHighestSalary |
+---------------------+
| null                |
+---------------------+
*/

SELECT  
    CASE WHEN rank_sal > 1 THEN salary 
    ELSE NULL END AS SecondHighestSalary 
FROM ( 
        SELECT  
        salary, 
        RANK() OVER (ORDER BY salary DESC) as rank_sal 
        FROM Employee 
        ORDER BY rank_sal  
    ) AS sub 
ORDER BY SecondHighestSalary DESC 
LIMIT 1;