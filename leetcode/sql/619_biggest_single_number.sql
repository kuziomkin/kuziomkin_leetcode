/*
Table: MyNumbers

+-------------+------+
| Column Name | Type |
+-------------+------+
| num         | int  |
+-------------+------+
This table may contain duplicates (In other words, there is no primary key for this table in SQL).
Each row of this table contains an integer.
 

A single number is a number that appeared only once in the MyNumbers table.

Find the largest single number. If there is no single number, report null.

The result format is in the following example.
*/

--My Solution
/*
Output 
- largest single num

Approach:
1. Count all num and filter only appeared once
2. Order By DESC and print first

Constraints:
- If there is no single number, report null
*/

WITH 
    agg_num AS (
        SELECT 
            num
            , COUNT(num) AS total_records
        FROM
            MyNumbers
        GROUP BY
            num
    )

SELECT
    CASE
        WHEN total_records = 1 THEN num
        ELSE NULL
    END AS num
FROM
    agg_num
ORDER BY
    num DESC
LIMIT 1