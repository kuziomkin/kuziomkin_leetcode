/*

Table: Customer
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| customer_id | int     |
| product_key | int     |
+-------------+---------+
This table may contain duplicates rows. 
customer_id is not NULL.
product_key is a foreign key (reference column) to Product table.

Table: Product

+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| product_key | int     |
+-------------+---------+
product_key is the primary key (column with unique values) for this table.
 
Write a solution to report the customer ids from the Customer table that bought all the products in the Product table.
Return the result table in any order.
The result format is in the following example.
*/

--My Solution
/*
Cardinality:
- Customer to Product as one to many

Output:
- customer_id who bought all products in the product table
- Result in any order

Approach:
- Count product_key in the Product
- Count Distinct product_key in the Customer per customer
- Define customer_id with all products
*/

--1.Count products
WITH
    agg_product AS (
        SELECT
            COUNT(*) AS total_products
        FROM
           Product 
    ),
--2.Count Distinct product_key in per customer
    agg_customer AS (
        SELECT
            customer_id
            , COUNT(DISTINCT product_key) AS total_products
        FROM
           Customer 
        GROUP BY
            customer_id
    )
--3.Define customers with all products
SELECT
    customer_id
FROM
    agg_customer
WHERE
    total_products IN (SELECT * FROM agg_product)
