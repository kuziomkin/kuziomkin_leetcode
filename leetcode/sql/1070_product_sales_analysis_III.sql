/*
Table: Sales

+-------------+-------+
| Column Name | Type  |
+-------------+-------+
| sale_id     | int   |
| product_id  | int   |
| year        | int   |
| quantity    | int   |
| price       | int   |
+-------------+-------+
(sale_id, year) is the primary key (combination of columns with unique values) of this table.
product_id is a foreign key (reference column) to Product table.
Each row of this table shows a sale on the product product_id in a certain year.
Note that the price is per unit.
 

Table: Product

+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| product_id   | int     |
| product_name | varchar |
+--------------+---------+
product_id is the primary key (column with unique values) of this table.
Each row of this table indicates the product name of each product.
 

Write a solution to select the product id, year, quantity, and price for the first year of every product sold.

Return the resulting table in any order.

*/

--My Solution
/* 
Cardinality:
- Product to Sales as one to many

Output:
- product_id
- year
- quantity
- price

Constraints
- first year of every product sold

Approach:
1. Define first year of every product sold (Window Function)
2. Filter out only first year records (Where clause)
*/

--1.Define first year of every product
WITH
    agg_sales AS (
        SELECT
            product_id
            , year
            , quantity
            , price
            , MIN(year) OVER (PARTITION BY product_id) AS min_year
        FROM
            Sales
    )

--2.Filter out records
SELECT
    product_id
    , year AS first_year
    , quantity
    , price
FROM
    agg_sales
WHERE
    year = min_year
