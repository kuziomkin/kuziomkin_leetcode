/*
Write an SQL query to rank the scores. The ranking should be calculated according to the following rules:

- The scores should be ranked from the highest to the lowest.
- If there is a tie between two scores, both should have the same ranking.
- After a tie, the next ranking number should be the next consecutive integer value. In other words, there should be no holes between ranks.
Return the result table ordered by score in descending order.

Table: Score
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| id          | int     |
| score       | decimal |
+-------------+---------+
id is the primary key for this table.
Each row of this table contains the score of a game.
Score is a floating point value with two decimal places.

Example:
Input: 
Scores table:
+----+-------+
| id | score |
+----+-------+
| 1  | 3.50  |
| 2  | 3.65  |
| 3  | 4.00  |
| 4  | 3.85  |
| 5  | 4.00  |
| 6  | 3.65  |
+----+-------+
Output: 
+-------+------+
| score | rank |
+-------+------+
| 4.00  | 1    |
| 4.00  | 1    |
| 3.85  | 2    |
| 3.65  | 3    |
| 3.65  | 3    |
| 3.50  | 4    |
+-------+------+
*/


-- Solution 1
SELECT  
    score, 
    DENSE_RANK() OVER (ORDER BY score DESC) AS 'rank' 
FROM Scores 
ORDER BY score DESC;


-- Solution 2
SELECT
    S1.score,
    (
        SELECT COUNT(DISTINCT S2.score)
        FROM Scores AS S2
        WHERE S2.score >= S1.score
    ) AS 'rank'
FROM Scores AS S1
ORDER BY S1.score DESC;