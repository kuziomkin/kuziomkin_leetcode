"""
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]
 

Constraints:

2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.

Follow-up: Can you come up with an algorithm that is less than O(n2) time complexity?

"""

# My solution 1
def twoSum(nums, target):
    result = []
    new_nums = nums[:]
    while(len(new_nums)>1):
        if (max(new_nums) + min(new_nums)) == target:
            pos1= nums.index(min(new_nums))
            if max(new_nums) in nums[pos1+1:]:
                pos2= nums[pos1+1:].index(max(new_nums)) + (pos1+1)
                result.append(pos1)
                result.append(pos2)
                break
            else:
                pos2= nums[:pos1].index(max(new_nums))
                result.append(pos1)
                result.append(pos2)
                break
        elif (max(new_nums) + min(new_nums)) > target:
            new_nums.remove(max(new_nums))
        elif (max(new_nums) + min(new_nums)) < target:
            new_nums.remove(min(new_nums))
    return result
    

# My soilution 2
def twoSum2(nums, targets):
    result = []
    for i in range(len(nums)):
        for n in range(len(nums)):
            if i == n:
                continue
            elif (nums[i] + nums[n]) == targets:
                result.append(i)
                result.append(n)
                break
        if len(result) > 0:
            break
    return result

# Proposed solution
def twoSum3(self, nums: List[int], target: int) -> List[int]:
    hashmap = {}
    for i in range(len(nums)):
        hashmap[nums[i]] = i
    for i in range(len(nums)):
        complement = target - nums[i]
        if complement in hashmap and hashmap[complement] != i:
            return [i, hashmap[complement]] 

