"""
Rank guests based on the number of messages they've exchanged with the hosts.
Guests with the same number of messages as other guests should have the same rank.
Do not skip rankings if the preceding rankings are identical.
Output the rank, guest id, and number of total messages they've sent.
Order by the highest number of total messages first.

airbnb_contacts

id_guest: object
id_host: object
id_listing: object
ts_contact_at: datetime64[ns]
ts_reply_at: datetime64[ns]
ts_accepted_at: datetime64[ns]
ts_booking_at: datetime64[ns]
ds_checkin: datetime64[ns]
ds_checkout: datetime64[ns]
n_guests: int64
n_messages: int64
"""


#My solution
import pandas as pd

df = airbnb_contacts

#calculate all messages from guest
df = df.groupby(by='id_guest').sum().reset_index()

#rank the guests
df['ranking'] = df['n_messages'].rank(method='dense', ascending=False)

#sort values
df.sort_values(by='ranking', inplace=True)

#display data
df[['ranking','id_guest','n_messages']]