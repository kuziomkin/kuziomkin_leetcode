"""
What were the top 10 ranked songs in 2010?
Output the rank, group name, and song name but do not show the same song twice.
Sort the result based on the year_rank in ascending order.

billboard_top_100_year_end

year: int64
year_rank: int64
group_name: object
artist: object
song_name: object
id: int64
"""


#My Solution
import pandas as pd

df = billboard_top_100_year_end

(df.loc[
    (df['year'] == 2010)
    & (df ['year_rank'] <= 10)]
    .drop_duplicates('song_name')
    .sort_values(by='year_rank', ascending=True)
    [['year_rank','group_name','song_name']]
    )