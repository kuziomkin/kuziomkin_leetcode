"""
Find the activity date and the pe_description of facilities with the name 'STREET CHURROS' 
and with a score of less than 95 points.

los_angeles_restaurant_health_inspections

serial_number: object
activity_date: datetime64[ns]
facility_name: object
score: int64
grade: object
service_code: int64
service_description: object
employee_id: object
facility_address: object
facility_city: object
facility_id: object
facility_state: object
facility_zip: object
owner_id: object
owner_name: object
pe_description: object
program_element_pe: int64
program_name: object
program_status: object
record_id: object
"""


#My solution
import pandas as pd

df = los_angeles_restaurant_health_inspections

#convert activity time to appropriate format
df['activity_date'] = df['activity_date'].dt.strftime('%Y-%m-%d')

#select data
(df.loc[
    (df['facility_name'] == 'STREET CHURROS')
    & (df['score'] < 95)]
    [['activity_date','pe_description']]
    )

