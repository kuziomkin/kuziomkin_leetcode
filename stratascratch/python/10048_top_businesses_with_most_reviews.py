"""
Find the top 5 businesses with most reviews. Assume that each row has a unique business_id such that the total reviews for each business is listed on each row.
Output the business name along with the total number of reviews and order your results by the total reviews in descending order.

yelp_business

business_id: object
name: object
neighborhood: object
address: object
city: object
state: object
postal_code: object
latitude: float64
longitude: float64
stars: float64
review_count: int64
is_open: int64
categories: object
"""


#My solution
import pandas as pd

df = yelp_business

df.sort_values(by='review_count', ascending=False)[['name','review_count']].head(5)
