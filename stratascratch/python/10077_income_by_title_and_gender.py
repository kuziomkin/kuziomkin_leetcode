"""
Find the average total compensation based on employee titles and gender.
Total compensation is calculated by adding both the salary and bonus of each employee. However, not every employee receives a bonus so disregard employees without bonuses in your calculation. Employee can receive more than one bonus.
Output the employee title, gender (i.e., sex), along with the average total compensation.


sf_employee

id: int64
first_name: object
last_name: object
age: int64
sex: object
employee_title: object
department: object
salary: int64
target: int64
email: object
city: object
address: object
manager_id: int64

sf_bonus

worker_ref_id: int64
bonus: int64
"""


#My solution

# Import your libraries
import pandas as pd

#rename column
sf_bonus.rename(columns={'worker_ref_id':'id'}, inplace=True)

#sumarize bonuses for the same employee
sf_bonus = sf_bonus.groupby(by='id').sum().reset_index()

#join tables
df = pd.merge(sf_employee, sf_bonus, on='id', how='inner')

#find compensation
df['compensation'] = df['salary'] + df['bonus']

#select data
(df[['employee_title', 'sex', 'compensation']]
    .groupby(by=['employee_title', 'sex'])
    .mean()
    .reset_index()
    .rename(columns={'compensation':'avg_total_comp'})
    )


