"""
Count the number of user events performed by MacBookPro users.
Output the result along with the event name.
Sort the result based on the event count in the descending order.

playbook_events

user_id: int64
occurred_at: datetime64[ns]
event_type: object
event_name: object
location: object
device: object

"""


# My Solution
import pandas as pd

df = playbook_events
(df.loc[df['device'] == 'macbook pro']
    .groupby('event_name')
    .count()
    .reset_index()
    .rename(columns={'user_id':'event_count'})
    .sort_values(by='event_count', ascending=False)
    [['event_name','event_count']]
    )