"""
Find the rate of processed tickets for each type.

facebook_complaints

complaint_id: int64
type: int64
processed: bool
"""


# My Solution
import pandas as pd

df = facebook_complaints

# Find amount of tickets
all_tickets = df.groupby(by='type').count().reset_index()
 
# Find amount of processed tickets
processed = (
    df.loc[df['processed'] == True]
    .groupby(by='type')
    .count()
    .reset_index()
    )

# Find processed rate
processed['processed_rate'] = processed['processed'] / all_tickets['processed']

# Display processed tickets rate
processed[['type','processed_rate']]
