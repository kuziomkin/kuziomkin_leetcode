"""
Find the base pay for Police Captains.
Output the employee name along with the corresponding base pay.

sf_public_salaries

id: int64
employeename: object
jobtitle: object
basepay: float64
overtimepay: float64
otherpay: float64
benefits: float64
totalpay: float64
totalpaybenefits: float64
year: int64
notes: datetime64[ns]
agency: object
status: object
"""


#My solution
import pandas as pd

df = sf_public_salaries

df.loc[df['jobtitle'] == 'CAPTAIN III (POLICE DEPARTMENT)'][['employeename', 'basepay']]