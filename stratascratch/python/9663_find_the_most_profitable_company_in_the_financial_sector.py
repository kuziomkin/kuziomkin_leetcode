"""
Find the most profitable company from the financial sector. 
Output the result along with the continent.

orbes_global_2010_2014

company: object
sector: object
industry: object
continent: object
country: object
marketvalue: float64
sales: float64
profits: float64
assets: float64
rank: int64
forbeswebpage: object

"""


#My solution
import pandas as pd

df = forbes_global_2010_2014

(df.loc[
    (df['sector'] == 'Financials')
    & (df['profits'] == df['profits'].max())]
    [['company','continent']]
    )