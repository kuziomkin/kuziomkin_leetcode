"""
Find matching hosts and guests pairs in a way that they are both of the same gender and nationality.
Output the host id and the guest id of matched pair.

airbnb_hosts

host_id: int64
nationality: object
gender: object
age: int64

airbnb_guests

guest_id: int64
nationality: object
gender: object
age: int64
"""


# My Solution
import pandas as pd

(pd.merge(airbnb_hosts, airbnb_guests, on=['gender','nationality'], how='inner')[['host_id','guest_id']]
    .drop_duplicates())