"""
Find the 3 most profitable companies in the entire world.
Output the result along with the corresponding company name.
Sort the result based on profits in descending order.

forbes_global_2010_2014

company: object
sector: object
industry: object
continent: object
country: object
marketvalue: float64
sales: float64
profits:float64
assets: float64
rank: int64
forbeswebpage:object
"""


#My solution
import pandas as pd

df = forbes_global_2010_2014

df[['company', 'profits']].sort_values(by='profits', ascending=False).head(3)