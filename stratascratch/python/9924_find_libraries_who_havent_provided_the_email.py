"""
Find libraries who haven't provided the email address in circulation year 2016 but their notice preference definition is set to email.
Output the library code.

library_usage

patron_type_code: int64
patron_type_definition: object
total_checkouts: int64
total_renewals: int64
age_range: object
home_library_code: object
home_library_definition: object
circulation_active_month: object
circulation_active_year: float64
notice_preference_code: object
notice_preference_definition: object
provided_email_address: bool
year_patron_registered: int64
outside_of_county: bool
supervisor_district: float64

"""


#My Solution
import pandas as pd

df = library_usage
df.loc[
    (df['provided_email_address'] == False)
    & (df['circulation_active_year'] == 2016)
    & (df['notice_preference_definition'] == 'email')]['home_library_code'].unique()
