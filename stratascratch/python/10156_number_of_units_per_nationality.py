"""
Find the number of apartments per nationality that are owned by people under 30 years old.
Output the nationality along with the number of apartments.
Sort records by the apartments count in descending order.

host_id: int64
nationality: object
gender: object
age: int64

host_id: int64
unit_id: object
unit_type: object
n_beds: int64
n_bedrooms: int64
country: object
city: object
"""


# My Solution
import pandas as pd

df = pd.merge(airbnb_hosts, airbnb_units, on='host_id', how='left' )

(df.loc[
    (df['age'] < 30) 
    & (df['unit_type'] == 'Apartment')]
    .drop_duplicates()
    .groupby('nationality')
    .count()
    .reset_index()[['nationality','host_id']]
)
