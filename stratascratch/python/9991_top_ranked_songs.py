"""
Find songs that have ranked in the top position. Output the track name and the number of times it ranked at the top. Sort your records by the number of times the song was in the top position in descending order.

DataFrame: spotify_worldwide_daily_song_rankingExpected Output Type: pandas.DataFrame

spotify_worldwide_daily_song_ranking

id: int64
position: int64
trackname: object
artist: object
streams: int64
url: object
date: datetime64[ns]
region: object

303651	52	Heart Won't Forget	Matoma	28047	https://open.spotify.com/track/2of2DM5LqTh7ohmmVXUKsH	2017-02-04 00:00:00	no
85559	160	Someone In The Crowd - From "La La Land" Soundtrack	Emma Stone	17134	https://open.spotify.com/track/7xE4vKvjqUTtHyJ9zi0k1q	2017-02-26 00:00:00	fr
1046089	175	The Greatest	Sia	10060	https://open.spotify.com/track/7xHWNBFm6ObGEQPaUxHuKO	2017-03-06 00:00:00	cl
350824	25	Unforgettable	French Montana	46603	https://open.spotify.com/track/3B54sVLJ402zGa6Xm4YGNe	2017-10-01 00:00:00	no
776822	1	Bad and Boujee (feat. Lil Uzi Vert)	Migos	1823391	https://open.spotify.com/track/4Km5HrUvYTaSUfiSGPJeQR	2017-01-27 00:00:00	us

"""


# My solution
import pandas as pd

(spotify_worldwide_daily_song_ranking[['trackname', 'id']][spotify_worldwide_daily_song_ranking['position'] == 1]
    .groupby('trackname')
    .size()
    .reset_index(name='times_top1')
    .sort_values('times_top1', ascending=False))