"""
Find how many times each artist appeared on the Spotify ranking list
Output the artist name along with the corresponding number of occurrences.
Order records by the number of occurrences in descending order.

spotify_worldwide_daily_song_ranking

id: int64
position: int64
trackname: object
artist: object
streams: int64
url: object
date: datetime64[ns]
region: object

"""


# My Solution
import pandas as pd

df = spotify_worldwide_daily_song_ranking

(df.groupby('artist')
    .count()
    .reset_index()[['artist', 'id']]
    .sort_values(by='id', ascending=False))