"""
Find the last time each bike was in use. Output both the bike number and the date-timestamp of the bike's last use (i.e., the date-time the bike was returned).
Order the results by bikes that were most recently used.

dc_bikeshare_q1_2012

duration: object
duration_seconds: int64
start_time: datetime64[ns]
start_station: object
start_terminal: int64
end_time: datetime64[ns]
end_station: object
end_terminal: int64
bike_number: object
rider_type: object
id: int64
"""


# My Solution
import pandas as pd

df = dc_bikeshare_q1_2012

(df.groupby('bike_number')
    .max()
    .reset_index()
    .rename(columns={'end_time':'last_used'})
    .sort_values(by='last_used', ascending=False)
    [['bike_number', 'last_used']]
    )