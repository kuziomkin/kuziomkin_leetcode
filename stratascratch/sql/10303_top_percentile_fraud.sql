/*
ABC Corp is a mid-sized insurer in the US and in the recent past their fraudulent claims have increased significantly for their personal auto insurance portfolio. 
They have developed a ML based predictive model to identify propensity of fraudulent claims.
Now, they assign highly experienced claim adjusters for top 5 percentile of claims identified by the model.
Your objective is to identify the top 5 percentile of claims from each state. Your output should be policy number,
state, claim cost, and fraud score.

fraud_score
- policy_num: varchar
- state: varchar
- claim_cost: int
- fraud_score: float


*/

WITH percentil_gr AS (
    SELECT 
        state
       , PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY fraud_score ASC)
    FROM
       fraud_score
    GROUP BY 
        state
)

SELECT 
    policy_num
    , fs.state
    , claim_cost
    , fraud_score
FROM
   fraud_score AS fs
LEFT JOIN percentil_gr AS pg
    ON fs.state = pg.state
WHERE
    fs.fraud_score >= pg.percentile_cont