/*
Find matching hosts and guests pairs in a way that they are both of the same gender and nationality.
Output the host id and the guest id of matched pair.

airbnb_hosts

host_id: int
nationality: varchar
gender: varchar
age: int

airbnb_guests

guest_id: int
nationality: varchar
gender: varchar
age: int

*/


-- My Solution
SELECT
    DISTINCT
    ah.host_id,
    ag.guest_id
FROM
    airbnb_hosts AS ah,
    airbnb_guests AS ag
WHERE
    ah.nationality = ag.nationality
    AND ah.gender = ag.gender