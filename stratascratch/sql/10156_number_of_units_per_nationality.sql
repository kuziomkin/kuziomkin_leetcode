/*
Find the number of apartments per nationality that are owned by people under 30 years old.
Output the nationality along with the number of apartments.
Sort records by the apartments count in descending order.

airbnb_hosts

host_id: int
nationality: varchar
gender: varchar
age: int

airbnb_units

host_id: int
unit_id: varchar
unit_type: varchar
n_beds: int
n_bedrooms: int
country: varchar
city: varchar
*/


-- My Solution
SELECT 
    ah.nationality,
    COUNT(au.host_id) AS apartment_count
FROM
    (
        SELECT 
            DISTINCT host_id,
            nationality,
            gender,
            age
        FROM
            airbnb_hosts
    ) AS ah
LEFT JOIN
    airbnb_units AS au
ON  
    ah.host_id = au.host_id
WHERE
    age < 30
    AND unit_type = 'Apartment'
GROUP BY
    nationality
ORDER BY
    apartment_count DESC